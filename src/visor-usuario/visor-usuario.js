import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .textoAzul {
            font-family: BentonSansBBVABold;
            font-size: 20px;
            font-weight: 700;
            line-height: 1.11;
            letter-spacing: -.3px;
            text-align: left;
            color: #004481;
        }

        .azulG {
            color: #004481;
            font-weight: bold;
            font-size: 17px;
        }

        .box-card {
            background: #fff;
            height: 95px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            justify-content: center;
            align-items: center;
            margin-left: 30px;
            padding-top: 30px;
          }

          .box-saldo {
              background: #fff;
              height: 95px;
              border-radius: 10px;
              border: 1px solid #dddfe2;
              padding-top: 5px;
              align-items: center;
              margin-left: 10px;

            }

        .head_box {
            background: transparent;
            font-size: 30px;
            border-bottom: 1px solid #dddfe2;
            margin-left: 30px;
            margin-right: 30px;
            padding: 15px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #0072c9;
            font-family: arial;
        }
        .verdeG {
            color: #268100;
            font-weight: bold;
            font-size: 24px;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div style="padding-bottom: 10px; background-color: #deecef">
      <div style="padding-top: 20px; margin-left:200px; margin-right:200px" hidden$="{{!logado}}">
        <span class="textoAzul" style="padding-bottom: 30px;">Posición global de  {{nombre}} {{apellido1}} {{apellido2}} </span>
        <div class="row" style="padding-top: 10px;">
            <div class="col-sm-5 ">
              <div class=" row box-saldo text-center ">
                <div class="col-sm-4 offset-1"><img src="/src/img/totalSaldo.png" /> </div>
                <div class="col-sm-4 offset-1"> <span class="verdeG"> Saldo total {{ _formatFix2(totalSaldo)}} €</span> </div>
              </div>
            </div>
            <div class="col-sm-6 offset-1 text-center box-area  ">
              <div class="row box-saldo text-center ">
              <div class="col-sm-3 "><img src="/src/img/saldoMes.png" /> </div>
              <div class="col-sm-3 offset-1">
                  <select class="custom-select" required value="{{mes::change}}" id="selectMes"></select>
                </div>
              <div class="col-sm-3 offset-1"> <span class="verdeG"> Saldo total {{ _formatFix2(saldoMes)}} €</span> </div>
              </div>
            </div>
        </div>

      </div>
</div>



      <iron-ajax

      id="getUser"
      url="http://localhost:3000/techUProyecto/users/{{idUsuario}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>

      <iron-ajax

      id="getTotalSaldo"
      url="http://localhost:3000/techUProyecto/cuentas/total/{{idUsuario}}"
      handle-as="json"
      on-response="totalSaldoUsuario"
      ></iron-ajax>

      <iron-ajax

      id="getTotalSaldoMes"
      url="http://localhost:3000/techUProyecto/totales/{{idUsuario}}"
      handle-as="json"
      on-response="totalSaldoMesUsuario"
      ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      nombre: {
        type: String
      }, apellido1: {
        type: String
      }, apellido2: {
        type: String
      }, email: {
        type: String
      }, logado : {
        type: Boolean,
        value: false
      }, idUsuario: {
        type: Number,
        value:window.location.search.substring(4)
      }, totalSaldo :{
        type: Number,
        value: 0
      }, saldoMes : {
        type: Number,
        value:0
      }, totalSaldoMes : {
        type : Array
      }, mes : {
        mes: String,
        value:"",
        observer : "_getSaldoMes"
      }, altaMovimiento : {
        type : Boolean,
        value : false,
        observer : "_altaOk"
      }
    };
  }// end properties

  //funcion que se ejecuta cuando se termina de cargar la pagina, con esto evito el auto y puedo prepara el req a enviar
    ready() {
      super.ready();
      if(sessionStorage.getItem("Authorization")){
        this.$.getUser.headers.authorization = sessionStorage.getItem("Authorization");
      }
      this.$.getUser.generateRequest(); //esto es para ejecutar la función.

    }

    _altaOk(newValue, oldValue) {
        if (newValue) {
          if(sessionStorage.getItem("Authorization")){
            this.$.getUser.headers.authorization = sessionStorage.getItem("Authorization");
          }
          this.$.getUser.generateRequest(); //esto es para ejecutar la función.

          this.altaMovimiento=false;
        }
    }

  showData(data) {

    if (!data.detail.response.logged){
      console.log(this);
      console.log(this.parent);
      //this.parent.userNoLogged();
      //this.sendEvent();
    } else {
      this.nombre = data.detail.response.nombre;
      this.apellido1 = data.detail.response.apellido1;
      this.apellido2 = data.detail.response.apellido2;
      this.email = data.detail.response.email;
      this.logado = data.detail.response.logged;
    }

    if(sessionStorage.getItem("Authorization")){
      this.$.getTotalSaldo.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.getTotalSaldo.generateRequest(); //esto es para ejecutar la función.
  }

    totalSaldoUsuario(data){
      this.totalSaldo = data.detail.response.total;

      if(sessionStorage.getItem("Authorization")){
        this.$.getTotalSaldoMes.headers.authorization = sessionStorage.getItem("Authorization");
      }
      this.$.getTotalSaldoMes.generateRequest(); //esto es para ejecutar la función.
    }

    _getSaldoMes(){
      if (this.mes!="") {
        for (var i=0; i<this.totalSaldoMes.length; i++){
          if (this.totalSaldoMes[i]._id==this.mes) {
            //recuperamos el saldo
            this.saldoMes = this.totalSaldoMes[i].total;
            break;

          }//end if

        }//end for
    }//end if

    }

    totalSaldoMesUsuario(data){
      this.totalSaldoMes = data.detail.response;
      //Inicializamos el combo con los meses recibidos
      var select = this.shadowRoot.getElementById('selectMes');
      select.innerHTML = "";
      for (var i=0; i<this.totalSaldoMes.length; i++){
        var mesTexto;
        switch (this.totalSaldoMes[i]._id) {
          case 1:
            mesTexto = "enero";
            break;
          case 2:
            mesTexto = "febrero";
            break;
          case 3:
            mesTexto = "marzo";
            break;
          case 4:
            mesTexto = "abril";
            break;
          case 5:
            mesTexto = "mayo";
            break;
          case 6:
            mesTexto = "junio";
            break;
          case 7:
            mesTexto = "julio";
            break;
          case 8:
            mesTexto = "agosto";
            break;
          case 9:
            mesTexto = "septiembre";
            break;
          case 10:
            mesTexto = "octubre";
            break;
          case 11:
            mesTexto = "noviembre";
            break;
          case 12:
            mesTexto = "diciembre";
            break;
          }
      select.innerHTML += "<option value='"+this.totalSaldoMes[i]._id+"'>"+mesTexto+"</option>";
        }//end for

        //inicializamos al mes en curso
        var d = new Date();
        this.mes = d.getMonth()+1;

    }//end ffuncion

    _formatFix2(valor){
      //return valor.toFixed(2);
      valor = new Intl.NumberFormat("es-ES").format(valor.toFixed(2));
      if (valor.indexOf(",")==-1){
          valor =   valor + ',00';
      }else {
        //miro los decimales
        if (valor.toString().length-valor.indexOf(",")<3)
        {
          valor =   valor + '0';
        }
      }

      return valor
    }

}//end class

window.customElements.define('visor-usuario', VisorUsuario);
