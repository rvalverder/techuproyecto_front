import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-input/paper-input.js';
import '@cwmr/paper-password-input/paper-password-input.js';



import '@polymer/paper-toggle-button/paper-toggle-button.js';

/**
 * @customElement
 * @polymer
 */
class EditarUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .error {
            color: #ef0707;
            font-weight: bold;
            font-size: 17px;
        }

        .azulG {
            color: #004481;
            font-weight: bold;
            font-size: 24px;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .azulL {
            color: #004481;
            font-weight: bold;
            font-size: 16px;
        }

        .azulClaro {
            color: #0072c9;
            font-weight: bold;
            font-size: 14px;
            cursor:pointer;
        }

        .caja {
            height: 60px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            justify-content: center;
            align-items: center;
            margin-left: 30px;
          }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <div style="padding-top: 15px; margin-left:400px; margin-right:400px" >
          <div class="row caja" style="background-color:#d4d3ea">
            <span class="azulG">perfil de usuario</span>
          </div>

        <div class="row justify-content-md-center" hidden$="{{altaOk}}">
          <div class="col-sm-10">
              <div class="mb-1" >
                <paper-input id="inputNombre" label="mombre" auto-validate  error-message="introduzca su nombre" value="{{nombre::input}}" required> </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input id="inputApellido" label="primer apellido" auto-validate  error-message="introduzca su primer apellido" value="{{apellido1::input}}" required> </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input label="segundo apellido"  value="{{apellido2::input}}">  </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input id="inputMail" label="email" disabled value="{{email::input}}" > </paper-input>
              </div>
              <div hidden$="{{!mailExist}}">
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >la dirección de email ya está registrada <span>
              </div>
              <div class="row">
                <div class="col-lg-4"><span id="recContraseña" class="azulClaro" > cambiar de contraseña</span></div>
                <div class="col-2"><paper-toggle-button id="cambiarPassword" on-click="mostrarCambioPass"></paper-toggle-button></div>

              </div>
              <div class="mb-1" hidden$="{{!changePass}}">
                <paper-password-input id="password" label="contraseña actual" auto-validate error-message="debe introducir un valor" required  value="{{password}}" on-click="limpiarPass"></paper-password-input>
              </div>
              <div hidden$="{{!passIncorrecta}}" >
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >contrseña incorrecta <span>
              </div>
              <div class="mb-1" hidden$="{{!changePass}}">
                <paper-password-input id="passwordNew" label="contraseña nueva" auto-validate error-message="debe introducir un valor" required  value="{{passwordNew}}" on-click="limpiarPass"></paper-password-input>
              </div>
              <div hidden$="{{!equalPassw}}" >
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >La nueva contraseña no puede ser igual a la anterior <span>
              </div>
              <div class="mb-1" hidden$="{{!changePass}}">
                <paper-password-input id="confirm" label="confirma nueva contraseña"  auto-validate error-message="debe introducir un valor" required value="{{passChequeo}}" on-click="limpiarPass"> </paper-password-input>
              </div>
              <div hidden$="{{!checkPassw}}" >
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >Las contraseñas no coinciden <span>
              </div>
              </div>

            </div>

            <div class="mb-4" style="padding-top: 15px;"  hidden$="{{altaOk}}">
              <div class="row caja" >
                  <div class="col-3" >
                    <button type="button" class="btn btn-outline-secondary" on-click="volver">cancelar</button>
                  </div>
                  <div class="col-2" >
                    <button type="button" class="btn btn-outline-success" on-click="aceptar">aceptar</button>
                  </div>
              </div>
            </div>


         </div>




         <div style="padding-top: 10px;  margin-left:400px; margin-right:400px" height=800px hidden$="{{!altaOk}}" >
          <div class="row caja" style="justify-content: center; height: 110px; background-color:#cbf3b3;" >
            <div style="justify-content: center;"  >
              <span class="azulL">Se ha modificado el perfil correctamente.<BR></span>
              <span class="azulL"><br>Pulse en el botón volver para acceder al listado de cuentas  </span>
            </div>
          </div>
        </div>



        <div style="padding-top: 10px; margin-left:400px; margin-right:400px" hidden$="{{!altaOk}}" >
          <div class="row caja" style="padding-top: 10px; justify-content: center;" >
            <button type="button" class="btn btn-outline-primary" on-click="volver">volver</button>
          </div>
        </div>


        <div style=" padding-top: 100px; margin-left:400px; margin-right:400px " hidden$="{{showErrorAuth}}">
          <div class="row caja" style="justify-content: center; height: 110px;" >
            <div style="justify-content: center;"  >
              <span class="redG">Acceso no autorizado</span>
            </div>
          </div>
        </div>


        <iron-ajax

        id="getUser"
        url="http://localhost:3000/techUProyecto/users/{{idUsuario}}"
        handle-as="json"
        on-response="showData"
        on-error="showError"
        ></iron-ajax>

        <iron-ajax
        id="doLogin"
        url="http://localhost:3000/techUProyecto/login"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="passwordOk"
        on-error="passwordKo"
        ></iron-ajax>

      <iron-ajax
      id="editarUsuario"
      url="http://localhost:3000/techUProyecto/users/{{idUsuario}}"
      handle-as="json"
      method="PUT"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      nombre : {
        type: String,
        value : ""
      },
      apellido1: {
        type: String,
        value : ""
      },
      apellido2: {
        type: String,
        value : ""
      },
      email: {
        type: String,
        value : ""
      },
      password: {
        type: String,
        value : ""
      },
      passChequeo: {
        type: String,
        value : ""
      }, passwordNew: {
        type: String,
        value : ""
      },altaOk: {
        type: Boolean,
        value: false
      },
      mailExist : {
        type: Boolean,
        value: false
      },
      checkPassw : {
        type: Boolean,
        value: false
      }, equalPassw :  {
        type: Boolean,
        value: false
      }, passIncorrecta : {
        type : Boolean,
        value : false
      }, idUsuario: {
        type: Number,
        value:window.location.search.substring(4)
      }, changePass : {
        type : Boolean,
        value : false
      }, showErrorAuth : {
        type:Boolean,
        value : true
      }


    };
  }// end properties

  ready() {
    super.ready();
    if(sessionStorage.getItem("Authorization")){
      this.$.getUser.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.getUser.generateRequest(); //esto es para ejecutar la función.

  }

  mostrarCambioPass(e) {
      if (this.shadowRoot.getElementById('cambiarPassword').checked)
      {
        this.changePass=true;
      }else {
        this.changePass=false;
      }

  }


  limpiarPass(){
    this.checkPassw=false;
    this.equalPassw=false;
    this.passIncorrecta=false;
  }


  aceptar (){
    this.checkPassw=false;
    this.equalPassw=false;
    this.mailExist=false;
    this.passIncorrecta=false;

    //Validamos que el formulario está completo
    if (this.changePass==true){
        if(this.shadowRoot.getElementById('inputNombre').validate() &&
         this.shadowRoot.getElementById('inputApellido').validate() &&
         this.shadowRoot.getElementById('password').validate()&&
         this.shadowRoot.getElementById('passwordNew').validate()&&
         this.shadowRoot.getElementById('confirm').validate())
         {

              if (this.passwordNew == this.passChequeo)
              {
                if (this.passwordNew != this.password)
                {
                    //1º comprobamos que la conmtraseña antigua es váloginData
                    var loginData = {
                      "email" : this.email,
                      "password" : this.password
                    }

                    this.$.doLogin.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
                    this.$.doLogin.generateRequest(); //esto es para ejecutar la función.

                } else {
                  this.equalPassw=true;
                }
              }
              else{
                this.checkPassw=true;
              }
          }//fin validación
          else{
            console.log("formulario incompleto");
          }
      }else{
        if(this.shadowRoot.getElementById('inputNombre').validate() &&
         this.shadowRoot.getElementById('inputApellido').validate())
         {
           var editionData = {
             "nombre" : this.nombre,
             "apellido1" : this.apellido1,
             "apellido2" : this.apellido2
           }
           if(sessionStorage.getItem("Authorization")){
             this.$.editarUsuario.headers.authorization = sessionStorage.getItem("Authorization");
           }
           this.$.editarUsuario.body = JSON.stringify(editionData); //para accder a la función ajax this.$ para acceder a elemento local
           this.$.editarUsuario.generateRequest(); //esto es para ejecutar la función.

         }else {
           console.log("formulario incompleto");
         }

      }
  }

  showData(data) {
    if (!data.detail.response.logged){
      //this.parent.userNoLogged();
      //this.sendEvent();
    } else {
      this.nombre = data.detail.response.nombre;
      this.apellido1 = data.detail.response.apellido1;
      this.apellido2 = data.detail.response.apellido2;
      this.email = data.detail.response.email;
      this.logado = data.detail.response.logged;
    }

  }

  //captura la respuesta del iron ajax
  manageAJAXResponse(data) {
    this.altaOk = true;

  }

  volver(e){
    this.altaOk=false;
    this.password=null;
    this.passwordNew=null;
    this.passChequeo=null;
    this.shadowRoot.getElementById('cambiarPassword').checked=false;
    this.changePass=false;
    this.dispatchEvent(
      new CustomEvent(
        "myevent", { }
      )
    )
  }

  passwordOk(){
    //se ejecuta la modificación
    var editionData = {
      "nombre" : this.nombre,
      "apellido1" : this.apellido1,
      "apellido2" : this.apellido2,
      "password" : this.passwordNew
    }
    if(sessionStorage.getItem("Authorization")){
      this.$.editarUsuario.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.editarUsuario.body = JSON.stringify(editionData); //para accder a la función ajax this.$ para acceder a elemento local
    this.$.editarUsuario.generateRequest(); //esto es para ejecutar la función.

  }

  passwordKo(){
    this.passIncorrecta=true;
  }

  //captura el error definido en el iron ajax
  showError(error)
  {
    this.verMov=false;

    //Si el error es 401  no autorizado
    if (error.detail.request.status==401 || error.detail.request.status==403){
     this.showErrorAuth=false;
    }

  }

}//end class

window.customElements.define('editar-usuario', EditarUsuario);
