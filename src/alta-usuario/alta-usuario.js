import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-input/paper-input.js';




import '@cwmr/paper-password-input/paper-password-input.js';
/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .error {
            color: #ef0707;
            font-weight: bold;
            font-size: 17px;
        }

        .azulG {
            color: #004481;
            font-weight: bold;
            font-size: 24px;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .azulL {
            color: #004481;
            font-weight: bold;
            font-size: 16px;
        }


        .caja {
            height: 60px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            justify-content: center;
            align-items: center;
            margin-left: 30px;
          }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <div style="padding-top: 15px; margin-left:400px; margin-right:400px" >
          <div class="row caja" style="background-color:#d4d3ea">
            <span class="azulG">alta usuario</span>
          </div>

        <div class="row justify-content-md-center" hidden$="{{altaOk}}">
          <div class="col-sm-10">
              <div class="mb-1" >
                <paper-input id="inputNombre" label="mombre" auto-validate  error-message="introduzca su nombre" value="{{nombre::input}}" required> </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input id="inputApellido" label="primer apellido" auto-validate  error-message="introduzca su primer apellido" value="{{apellido1::input}}" required> </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input label="segundo apellido"  value="{{apellido2::input}}">  </paper-input>
              </div>
              <div class="mb-1" >
                <paper-input id="inputMail" label="email" auto-validate pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}" error-message="introduzca email correcto" required value="{{email::input}}" on-click="limpiarEmail"> </paper-input>
              </div>
              <div hidden$="{{!mailExist}}">
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >la dirección de email ya está registrada <span>
              </div>
              <div class="mb-1" >
                <paper-password-input id="password" label="contraseña" auto-validate error-message="debe introducir un valor" required  value="{{password}}" on-click="limpiarPass"></paper-password-input>
              </div>
              <div class="mb-1" >
                <paper-password-input id="confirm" label="confirma contraseña"  auto-validate error-message="debe introducir un valor" required value="{{passChequeo}}" on-click="limpiarPass"> </paper-password-input>
              </div>
              <div hidden$="{{!checkPassw}}" >
                <iron-icon icon="report"  style="fill:red" ></iron-icon>
                <span  class="error" >Las contraseñas no coinciden <span>
              </div>
              </div>

            </div>

            <div class="mb-4" style="padding-top: 15px;"  hidden$="{{altaOk}}">
              <div class="row caja" >
                  <div class="col-3" >
                    <button type="button" class="btn btn-outline-secondary" on-click="volver">cancelar</button>
                  </div>
                  <div class="col-2" >
                    <button type="button" class="btn btn-outline-success" on-click="aceptar">aceptar</button>
                  </div>
              </div>
            </div>


         </div>




         <div style="padding-top: 10px;  margin-left:400px; margin-right:400px" height=800px hidden$="{{!altaOk}}" >
          <div class="row caja" style="justify-content: center; height: 110px; background-color:#cbf3b3;" >
            <div style="justify-content: center;"  >
              <span class="azulL">Se ha realizado el alta de {{nombre}} {{apellido1}} {{apellido2}} correctamente.</span>
              <span class="azulL">Pulse en el botón acceder para logarse en el sistema  </span>
            </div>
          </div>
        </div>



        <div style="padding-top: 10px; margin-left:400px; margin-right:400px" hidden$="{{!altaOk}}" >
          <div class="row caja" style="padding-top: 10px; justify-content: center;" >
            <button type="button" class="btn btn-outline-primary" on-click="irLogin">acceder</button>
          </div>
        </div>





      <iron-ajax
      id="crearUsuario"
      url="http://localhost:3000/techUProyecto/users"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      nombre : {
        type: String,
        value : ""
      },
      apellido1: {
        type: String,
        value : ""
      },
      apellido2: {
        type: String,
        value : ""
      },
      email: {
        type: String,
        value : ""
      },
      password: {
        type: String,
        value : ""
      },
      passChequeo: {
        type: String,
        value : ""
      },

      altaOk: {
        type: Boolean,
        value: false
      },
      mailExist : {
        type: Boolean,
        value: false
      },
      checkPassw : {
        type: Boolean,
        value: false
      }


    };
  }// end properties

  aceptar (){

    this.checkPassw=false;
    this.mailExist=false;

    //Validamos que el formulario está completo
    if(this.shadowRoot.getElementById('inputNombre').validate() &&
     this.shadowRoot.getElementById('inputApellido').validate() &&
     this.shadowRoot.getElementById('inputMail').validate()&&
     this.shadowRoot.getElementById('password').validate()&&
     this.shadowRoot.getElementById('confirm').validate())
     {

          if (this.password == this.passChequeo)
          {
            var loginData = {
              "nombre" : this.nombre,
              "apellido1" : this.apellido1,
              "apellido2" : this.apellido2,
              "email" : this.email,
              "password" : this.password
            }


            this.$.crearUsuario.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
            this.$.crearUsuario.generateRequest(); //esto es para ejecutar la función.

          }
          else{
            this.checkPassw=true;
          }
      }//fin validación
  }

  irLogin(){
    this.altaOk=false;
    this.nombre = "";
    this.apellido1 = "";
    this.apellido2 = "";
    this.email = "";
    this.password = "";
    this.passChequeo = "";
    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "destino" : "login-usuario"
          }
        }
      )
    )
  }

  limpiarEmail(){
    this.mailExist=false;
  }

  limpiarPass(){
    this.checkPassw=false;
  }

  //captura el error definido en el iron ajax
  showError(error)
  {
    console.log(error);


   if (error.detail.request.status==403){
     this.mailExist  = true;
   }
  }

  //captura la respuesta del iron ajax
  manageAJAXResponse(data) {
    this.altaOk = true;

  }

  volver(e){
    this.dispatchEvent(
      new CustomEvent(
        "myevent", {
          "detail" : {
            "destino" : "panel-acceso"
          }
        }
      )
    )
  }

}//end class

window.customElements.define('alta-usuario', AltaUsuario);
