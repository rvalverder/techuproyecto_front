import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@vaadin/vaadin-grid/vaadin-grid-sort-column.js';




/**
 * @customElement
 * @polymer
 */

class VisorMovivimientos extends PolymerElement {
  static get template() {
    return html`


      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .caja {
            height: 40px;
            border-radius: 0px;
            border: 1px solid #dadfe4;
            justify-content: center;
            align-items: center;
            margin-left: 30px;
          }

          .cabecera {
              color: #fff;
              font-weight: bold;
              font-size: 24px;
          }

          .mensaje {
            font-style: italic;

            color: grey;
            font-size: 24px;
          }

      </style>


<div style="padding-top: 20px; " >

<div class="row caja" style="justify-content: center; padding-top: 10px; margin-left:30px; margin-right:30px; background-color:#0862da">
  <span class="cabecera" style="margin-left:30px; margin-right:30px">Listado de movimientos </span>
  <span class="cabecera" style="margin-left:10px; "> {{IBAN}}</span>
</div>

  <div style="padding-bottom: 20px; padding-top: 10px; margin-left:30px; margin-right:30px" >

  <vaadin-grid theme="wrap-cell-content" id="tablaMovimientos" hidden$={{sinMovimientos}}>
     <vaadin-grid-sort-column path="fechaMostrar" width="120px" flex-grow="0" header="fecha"></vaadin-grid-sort-column>
     <vaadin-grid-sort-column path="tipoMovimiento" width="220px" flex-grow="0"  header="tipo de movimiento" ></vaadin-grid-sort-column>
     <vaadin-grid-sort-column path="concepto"  header="concepto"  flex-grow="5"  ></vaadin-grid-sort-column>
     <vaadin-grid-sort-column path="importe" header="importe" text-align="end"  flex-grow="2"></vaadin-grid-sort-column>
   </vaadin-grid>

   <div class="row mensaje"  hidden$={{!sinMovimientos}} style="padding-top: 30px; margin-left:270px;"> <span>cuenta sin movimientos </span> </div>
 </div>
</div>

      <iron-ajax
      id="getMovimientos"
      url="http://localhost:3000/techUProyecto/movimientos/cuenta/{{IBAN}}/{{iduser}}"
      handle-as="json"
      content-type="application/json"
      on-response="showMovimientos"
      on-error="showError"
      ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      IBAN: {
        type: String,
        value:"",
        observer : "_ctaChanged"
      }, iduser: {
        type: String,
        value:window.location.search.substring(4)
      }, sinMovimientos : {
        type: Boolean,
        value: false
      }
    };
  }// end properties


  _ctaChanged(newValue, oldValue) {
      if (newValue) {
        this.sinMovimientos = false;
        if(sessionStorage.getItem("Authorization")){
          this.$.getMovimientos.headers.authorization = sessionStorage.getItem("Authorization");
        }
        this.$.getMovimientos.generateRequest(); //esto es para ejecutar la función.

      }
    }


  showMovimientos(data) {
    this.sinMovimientos = false;
    this.movimientos = data.detail.response;

    for (var i=0; i<this.movimientos.length; i++)
    {

      this.movimientos[i].importe = new Intl.NumberFormat("es-ES").format(this.movimientos[i].importe);

      if (this.movimientos[i].importe.indexOf(",")==-1){

          this.movimientos[i].importe =   this.movimientos[i].importe + ',00';
      }else {
        if (this.movimientos[i].importe.toString().length-this.movimientos[i].importe.indexOf(",")<3)
        {
          this.movimientos[i].importe =   this.movimientos[i].importe + '0';
        }
      }
    }

        //Pintamos la tabla
       const grid = this.shadowRoot.querySelector('vaadin-grid');
       const columns = this.shadowRoot.querySelectorAll('vaadin-grid-sort-column');
       grid.items = data.detail.response;

       grid.addEventListener('active-item-changed', function(event) {
         const item = event.detail.value;
         grid.selectedItems = item ? [item] : [];

       });


  }


  //captura el error definido en el iron ajax
  showError(error)
  {
    this.movimientos = null;
    this.IBAN="";
    this.sinMovimientos = true;

  }

}//end class

window.customElements.define('visor-movimientos', VisorMovivimientos);
