import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .azulL {
            color: #004481;
            font-weight: bold;
            font-size: 16px;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .caja {
            height: 60px;
            border-radius: 0px;
            border: 1px solid #42960f;
            justify-content: center;
            align-items: center;
            margin-left: 30px;


          }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <div style="padding-top: 70px; margin-left:400px; margin-right:400px"  >

          <div class="row caja" style="padding-top: 7px; " >
              <span class="azulL">Confirmación logout</span>
          </div>
      </div>

      <div style=" padding-top: 0px; margin-left:400px; margin-right:400px " >
        <div class="row caja" style="justify-content: center; height: 110px; background-color:#cbf3b3;" >
          <div style="justify-content: center;"  >
            <span class="azulL">¿Desea cerrar la sesión actual de TechUBank?</span>
          </div>
        </div>
    </div>

    <div style="margin-left:400px; margin-right:400px"  >

        <div class="row caja" >
        <div class="col-2 offset-8" >
          <button type="button" class="btn btn-outline-secondary" on-click="volver">cancelar</button>
        </div>
        <div class="col-2" >
          <button type="button" class="btn btn-outline-success" on-click="logout">aceptar</button>
        </div>
        </div>
    </div>





      <iron-ajax
      id="doLogout"
      url="http://localhost:3000/techUProyecto/logout/{{idUsuario}}"
      handle-as="json"
      method="POST"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      idUsuario: {
        type: String,
        value:window.location.search.substring(4)
    }
  }

  }// end properties

  logout(){
    if(sessionStorage.getItem("Authorization")){
      this.$.doLogout.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.doLogout.generateRequest(); //esto es para ejecutar la función.

  }

  volver (e) {
      this.dispatchEvent(
        //clase predefinida con parámetros:
        //  - primero el nombre el que queramos [MINÚSCULAS siempre]
        //  - object que es lo que tiene dentro el evento "detail" con otro objeto dentro que tiene lo que queramos compartir
        new CustomEvent(
          "myevent",
          {
            "detail" : {
              "cancelar" : "true"
            }
          }
        )
      )
  }

  //captura el error definido en el iron ajax
  showError(error)
  {
    console.log("algo ha ido mal amigo en el logaout");
    console.log(error);
    if (error.detail.request.status==401 || error.detail.request.status==403){
      var token = null;
      sessionStorage.setItem("Authorization", "Bearer "+token);
      window.location="/index.html";
    }

  }

  manageAJAXResponse () {
      var token = null;
    sessionStorage.setItem("Authorization", "Bearer "+token);
    window.location="/index.html";
}

}//end class

window.customElements.define('logout-usuario', LogoutUsuario);
