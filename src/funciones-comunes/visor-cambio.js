import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class VisorCambio extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }


        .box-card {
            margin-top:20px;
            background: #fff;
            height: 130px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            -webkit-box-shadow: 0px 0px 20px rgba(140, 158, 255, 1);
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .head_box {
            background: transparent;
            font-size: 18px;
            border-right: 2px solid #dddfe2;
            padding: 15px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #0072c9;
            font-family: arial;
        }
        .divisas_box {
            background: transparent;
            font-size: 20px;
            padding: 5px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #0072c9;
            font-family: arial;
        }
        .body_box {
            background: transparent;
            font-size: 16px;
            padding: 15px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #004481;
            font-family: arial;
        }
        .nombreMoneda {
            background: transparent;
            font-size: 13px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #004481;
            font-family: arial;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">



       <div class="box align-left"  >
          <div class="row box-card">
              <div class="head_box col-3 text-center pointer relative" >
                <span> Cambio divisa {{fecha}} </span><br><br>

                  <select class="custom-select" required value="{{moneda::change}}" id="selectDivisa"></select>

              </div>

              <dom-repeat items="{{divisas}}">
                <template>
                    <div class="divisas_box col-xs-2 text-center pointer relative" style=" width: 155px;" on-change=(recuperaDivisa) >
                      <img src="/src/img/{{item.imagen}}"  height="50" width="50"/><br>
                      <span class="body_box"> {{item.to}}  </span><br>
                      <span class="nombreMoneda"> {{item.nombre}}</span><br>
                      <span style="padding-top:10px"> {{item.rate}} € </span>
                    </div>
                </template>
              </dom-repeat>

          </div>

       </div>



       <iron-ajax

       id="getCambio"
       url="http://localhost:3000/techUProyecto/cambio/{{moneda}}"
       handle-as="json"
       on-response="showDivisas"
       on-error="showError"
       ></iron-ajax>



  `;
  }

  static get properties() {
    return {
      divisas: {
        type: Array
      },divisasCombo: {
        type: Array
      }, fecha : {
        type: String
      }, moneda : {
        type : String,
        value : "EUR",
        observer : "_recuperaDivisa"
      }
    };
  }// end properties

  ready() {
    super.ready();
    //Recuperamos y formateamos la fecha del día.
    var todayTime = new Date();
    var month = todayTime .getMonth() + 1;
    var mesTexto;
    var day = todayTime .getDate();
    var year = todayTime .getFullYear();

    switch (month) {
      case 1:
        mesTexto = "enero";
        break;
      case 2:
        mesTexto = "febrero";
        break;
      case 3:
        mesTexto = "marzo";
        break;
      case 4:
        mesTexto = "abril";
        break;
      case 5:
        mesTexto = "mayo";
        break;
      case 6:
        mesTexto = "junio";
        break;
      case 7:
        mesTexto = "julio";
        break;
      case 8:
        mesTexto = "agosto";
        break;
      case 9:
        mesTexto = "septiembre";
        break;
      case 10:
        mesTexto = "octubre";
        break;
      case 11:
        mesTexto = "noviembre";
        break;
      case 12:
        mesTexto = "diciembre";
        break;
      }

    this.fecha = day + " de " + mesTexto + " de " + year;

    var divisasRec = [{
      "divisa" : "ARG",
      "nombre" : "peso argentino"},
      { "divisa" : "COP",
      "nombre" : "peso colombiano"},
      {"divisa" : "EUR",
      "nombre" : "euro europeo"},
      { "divisa" : "MXN",
      "nombre" : "peso mexicano"},
      { "divisa" : "PEN",
      "nombre" : "sol peruano"},
      { "divisa" : "UYU",
      "nombre" : "peso uruguayo",},
      { "divisa" : "USD",
      "nombre" : "dolar americano"},
      { "divisa" : "TRY",
      "nombre" : "lira turca" }];

    var select = this.shadowRoot.getElementById('selectDivisa');
    select.innerHTML = "";
    for (var i=0; i<divisasRec.length; i++){
      if (divisasRec[i].divisa=="EUR"){
        select.innerHTML += "<option value='"+divisasRec[i].divisa+"' selected >" +divisasRec[i].nombre+"</option>";
      } else {
        select.innerHTML += "<option value='"+divisasRec[i].divisa+"' > "+divisasRec[i].nombre+"</option>";
      }
    }

    this.$.getCambio.generateRequest();

  }

  showDivisas(data){
    this.divisas = new Array();

    // recorremos para añadir imagen y descripcion
    for (var i=0; i<data.detail.response.length; i++){
      if (data.detail.response[i].to=="ARS"){
        data.detail.response[i].nombre="peso argentino";
        data.detail.response[i].imagen="argentina.png";

      } else if (data.detail.response[i].to=="COP"){
        data.detail.response[i].nombre = "peso colombiano";
        data.detail.response[i].imagen="colombia.png";

      } else if (data.detail.response[i].to=="EUR"){

        data.detail.response[i].nombre="euro europeo";
        data.detail.response[i].imagen="europa.png";

      } else if (data.detail.response[i].to=="MXN"){
        data.detail.response[i].nombre="peso mexicano";
        data.detail.response[i].imagen="mexico.png";

      } else if (data.detail.response[i].to=="PEN"){
        data.detail.response[i].nombre="sol peruano";
        data.detail.response[i].imagen="peru.png";

      } else if (data.detail.response[i].to=="UYU"){
        data.detail.response[i].nombre="peso uruguayo";
        data.detail.response[i].imagen="uruguay.png";

      } else if (data.detail.response[i].to=="USD"){
        data.detail.response[i].nombre="dolar americano";
        data.detail.response[i].imagen="usa.png";

      } else if (data.detail.response[i].to=="TRY"){
        data.detail.response[i].nombre="lira turca";
        data.detail.response[i].imagen="turkey.png";

      }

    }//for

      this.divisas = data.detail.response;

  }

  _recuperaDivisa(){
    if (this.moneda!=""){
      this.$.getCambio.generateRequest();
    }

  }




  showError(error)
  {
    console.log(error);

  }



}//end class

window.customElements.define('visor-cambio', VisorCambio);
