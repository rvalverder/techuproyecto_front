import {PolymerElement, html} from '@polymer/polymer';
import '@polymer/paper-dialog/paper-dialog.js';

class modalWindow extends PolymerElement {
  static get template() {
    return html`

    <style>

        .azulG {
          color: #004481;
          font-weight: bold;
          font-size: 24px;
        }

        .textoMensajes {
          background: transparent;
          font-weight: bold;
          font-size: 17px;
          color: #004481;
          font-family: arial;
        }

        .confirmCaja {
            justify-content: center;
            align-items: center;
          }


    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <paper-dialog id="animated"  with-backdrop>
      <div style="justify-content: center;align-items: center; margin-left:30px; margin-right:30px; width=300px">
          <div class="row confirmCaja" style="border-bottom: 1px solid #dddfe2;">
            <span class="azulG" style=" padding-bottom: 10px;">Confirmación</span>
          </div>
          <div class="row confirmCaja" style=" padding-top: 30px; ">
            <span class="textoMensajes">{{textoConfirmacion1}}</span>
          </div>
          <div class="row confirmCaja" style=" padding-top: 10px;">
            <span class="textoMensajes">{{textoConfirmacion2}}</span>
          </div>
          <div class="row" style=" padding-top: 40px;">
                <div class="col-3 offset-6" >
                  <button type="button" class="btn btn-outline-secondary" dialog-dismiss >cancelar</button>
                </div>
                <div class="col-2" >
                  <button type="button" class="btn btn-outline-success" dialog-confirm autofocus on-tap="confirmar">aceptar</button>
                </div>
          </div>
        </div>

      </div>
    </paper-dialog>

    <paper-dialog id="alert"  with-backdrop>
      <div style="justify-content: center;align-items: center; margin-left:30px; margin-right:30px; width=300px">
          <div class="row confirmCaja" style="border-bottom: 1px solid #dddfe2;">
            <span class="azulG" style=" padding-bottom: 10px;">Aviso</span>
          </div>
          <div class="row confirmCaja" style=" padding-top: 30px; ">
            <span class="textoMensajes">{{textoConfirmacion1}}</span>
          </div>
          <div class="row confirmCaja" style=" padding-top: 40px;">
            <button type="button" class="btn btn-outline-secondary" dialog-dismiss >Cerrar</button>
          </div>
        </div>

      </div>
    </paper-dialog>


    `;
  }

  static get properties() {
    return {
      textoConfirmacion1 : {
        type : String,
        value : ""
      }, textoConfirmacion2 : {
        type : String,
        value : ""
      }, accionOption : {
        type : String,
        value :""
      }
    };
  }// end properties

  confirmar(e){
    this.dispatchEvent(
      new CustomEvent(
        "myevent", {}
      )
    )

  }
}
customElements.define('ventana-avisos', modalWindow);
