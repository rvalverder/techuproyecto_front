import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/iron-icons.js';
 import '@polymer/paper-input/paper-input.js';
//import '@cwmr/paper-password-input/paper-password-input.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .application-title {
            font-family: BentonSansBBVABold;
            font-size: 28px;
            font-weight: 700;
            line-height: 1.11;
            letter-spacing: -.3px;
            text-align: left;
            color: #004481;
        }

        .azulClaro {
            color: #0072c9;
            font-weight: bold;
            font-size: 14px;
            cursor:pointer;
        }

        .redG {
            color: #ef0707;
            font-weight: bold;
            font-size: 14px;
        }

        .error {
            color: #ef0707;
            font-weight: bold;
            font-size: 17px;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .cont_Login {
            background: transparent;
            font-size: 13px;
            border: 2px solid #004481;
            margin-left: 15px;
            padding: 15px;
        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <div class="row justify-content-md-center">
          <div class="application-title ng-binding" style="float: left; padding-top: 50px; padding-bottom:30px;">Login Usuario</div>
        </div>

        <custom-style>
          <style is="custom-style">
            paper-icon-button.red {
              width: 35px;
              height: 35px;
              color: red;
            }
          </style>
        </custom-style>


        <div class="row justify-content-md-center">
          <div class="cont_Login col-sm-5">
            <div class="row justify-content-md-center" style="margin-top:40px" >
              <paper-input id="inputMail" label="email" auto-validate pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}" error-message="introduzca email correcto" required value="{{email::input}}" on-focus="inicializaSpan">
              <paper-icon-button slot="suffix" on-click="clearInput" icon="clear" class="red" alt="limpiar" title="clear" >
              </paper-icon-button>
              </paper-input>
            </div>

            <div class="row justify-content-md-center" >
                <paper-password-input id="password" label="contraseña"  error-message="debe de tener al menos 6 caracteres" required  value="{{password}}" on-focus="inicializaSpan"></paper-password-input>
            </div>
            <div class="row justify-content-md-center" style="margin-top:20px">
                <iron-icon icon="report"  style="fill:red" hidden$="{{!isLogged}}"></iron-icon>
                <span id="recContraseña" class="azulClaro" on-click="recuperarConstraseña"> ¿olvidaste tu contraseña de acceso?</span>
            </div>


            <div class="row justify-content-md-center" style="margin-top:40px">
            <button type="button" class="btn btn-outline-primary" on-click="login">acceder</button>

            </div>
          </div>
         </div>






      <iron-ajax
      id="doLogin"
      url="http://localhost:3000/techUProyecto/login"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"


      ></iron-ajax>

      <iron-ajax
      id="sendPassword"
      url="http://localhost:3000/techUProyecto/password/{{email}}"
      handle-as="json"
      method="GET"
      content-type="application/json"
      on-response="passwordRegenerada"
      on-error="showError"


      ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      email: {
        type: String,
        value:""
      }, password: {
        type: String,
        value:""
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }// end properties

  login (){

    if(this.shadowRoot.getElementById('inputMail').validate() &&
     this.shadowRoot.getElementById('password').validate()){

        //montamos el body donde van los parametros que enviamos al POST
        var loginData = {
          "email" : this.email,
          "password" : this.password
        }

        this.$.doLogin.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
        this.$.doLogin.generateRequest(); //esto es para ejecutar la función.

      }

  }

  inicializaSpan(){
      this.isLogged = false;
      this.shadowRoot.getElementById('recContraseña').className="azulClaro";
      this.shadowRoot.getElementById('recContraseña').innerText="¿olvidaste tu contraseña de acceso?";
    //}
  }

  recuperarConstraseña(){
    if (this.email!="" && this.shadowRoot.getElementById('inputMail').validate()){
      this.$.sendPassword.generateRequest(); //esto es para ejecu
    } else {
      this.shadowRoot.getElementById('recContraseña').className="redG";

      //this.shadowRoot.getElementById('recContraseña').addClass('class', 'redG');
      this.shadowRoot.getElementById('recContraseña').innerText="debe introducir una dirección de email correcta";
    }

  }

  passwordRegenerada(){
     this.shadowRoot.getElementById('recContraseña').innerText="se ha enviado nueva contraseña al email " + this.email;
  }

  clearInput(){
    this.email = "";
  }



  //captura el error definido en el iron ajax
  showError(error)
  {
    if(error.target.id=="sendPassword"){
      this.shadowRoot.getElementById('recContraseña').innerText="se ha enviado nueva contraseña al email " + this.email;
    } else  {
      this.shadowRoot.getElementById('recContraseña').className="error";
      this.shadowRoot.getElementById('recContraseña').innerText="usuario y/o contraseña incorrectos";
      this.isLogged = true;
    }



  }

  //captura la respuesta del iron ajax
  manageAJAXResponse(data) {
    //el metodo al logar al usuairo genera el token y lo guarda en en response para la siguiente llamada
    var token = data.detail.response.token;
    //como voy a cambiar de pagina y pierdo el request lo guardo en session
    sessionStorage.setItem("Authorization", "Bearer "+token);

    window.location="/principal.html?id:"+data.detail.response.idUsuario;

  }



  sendEvent(e){
    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "id" : "1"
          }
        }

      )
    )
  }




}//end class

window.customElements.define('login-usuario', LoginUsuario);
