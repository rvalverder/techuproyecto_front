import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../panel-acceso/panel-acceso.js';
import '../login-usuario/login-usuario.js';
import '../alta-usuario/alta-usuario.js';




/**
 * @customElement
 * @polymer
 */
class HomeApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
        .cabecera {
            background:  #004481;
            height: 100px;
            margin:0px;
            border:0px;
            justify-content: center;
            align-items: center;
            width: 99%;
            margin: 0 auto;
        }

        .bluebg {
          background-color: #004481;

        }

        .enlacesCabecera{
          cursor:pointer;
          font-size: 17px;
          font-weight: bold;
          color: #ffffff;
          font-family: arial;
        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div  class="bluebg">
      <div class="row cabecera">
        <div class="col-2  col-sm-6 " on-click="home">
          <img src="/src/img/techu.jpg" />
        </div>
        <div class="col-3 offset-3 col-sm-1 ">
          <span  class="enlacesCabecera" on-click="openHelp"> Ayuda</span>
        </div>
        <div class="col-2 col-sm-1 ">
          <span class="enlacesCabecera" on-click="alta" > Registro</span>
        </div>
        <div class="col-3 col-sm-1 " >
        <span class="enlacesCabecera"  on-click="login" > Login</span>
        </div>
      </div>
  </div>

          <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
              <div component-name="panel-acceso"> <panel-acceso ></panel-acceso></div>
              <div component-name="alta-usuario"> <alta-usuario on-myevent="processEvent"></alta-usuario></div>
              <div component-name="login-usuario"> <login-usuario ></login-usuario></div>

          </iron-pages>

  `;
  }

  static get properties() {
    return {
      componentName : {
        type : String,
        value : "panel-acceso"
      }

    };
  }// end properties


  login(){
    this.componentName="login-usuario";
  }

  alta(){
    this.componentName="alta-usuario";
  }

  home(){
    this.componentName="panel-acceso";
  }

  processEvent(e){
      this.componentName = e.detail.destino;

    }

  openHelp(e){

      window.open("/src/hlp/TechU-Bank Manual de Usuario.pdf")
  }

}//end class

window.customElements.define('home-app', HomeApp);
