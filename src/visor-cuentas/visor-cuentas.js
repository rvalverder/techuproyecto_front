import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-accordion/vaadin-accordion.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../alta-movimientos/alta-movimientos.js'
import '../visor-movimientos/visor-movimientos.js'
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/polymer/lib/elements/dom-if.js'
import '../funciones-comunes/ventana-avisos.js'



/**
 * @customElement
 * @polymer
 */

class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .redG {
            color: #ef0707;
            font-weight: bold;
            font-size: 24px;
        }
        .azulG {
            color: #004481;
            font-weight: bold;
            font-size: 24px;
        }

        .textoCuentas {
            background: transparent;
            font-size: 15px;
            color: #004481;
            font-family: arial;
        }

        .tabla {
            height: 40px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            justify-content: center;
            align-items: center;
            margin-left: 30px;

          }

        .caja {
            height: 60px;
            border-radius: 0px;
            border: 1px solid red;
            justify-content: center;
            align-items: center;
            margin-left: 30px;


          }

          .textoCuentasRojo {
              color: red;
          }



      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div hidden$="{{!showErrorAuth }}">
<div style="padding-top: 30px; margin-left:30px; margin-right:30px"  hidden$="{{verCuentas}}"  >
<div class="row">

</div>
<div class="row">

  <div class="col-3" style="text-align: left;">
    <span class="azulG" style="padding-top: 10px; margin-left:30px; margin-right:30px">Listado de cuentas</span>
  </div>
  <div class="col-2" style="text-align: right;">
    <button class="btn btn-primary  enter" on-click="openConfirm">Crear cuenta</button>
    </div>
</div>

<div class="row "   >
<div class="col-5">
<div style="padding-top: 10px; margin-left:30px; margin-right:30px" >
    <dom-repeat items="{{accounts}}">
      <template >
          <div  style="padding-top: 5px;">
            <div class="row tabla"    id="rowTablaCuenta"   >
            <template is="dom-if" if="{{!_saldoNegativo(item.balance)}}">
              <div class="textoCuentas col-sm-6" >  <span id="cuentaSel" > {{item.IBAN}} </span> </div>
              <div class="textoCuentas col-sm-3" style="text-align: right;">  <span> {{ _formatFix2(item.balance)}} € </span> </div>
            </template>
            <template is="dom-if" if="{{_saldoNegativo(item.balance)}}">
              <div class="textoCuentasRojo col-sm-6" >  <span id="cuentaSel" > {{item.IBAN}} </span> </div>
              <div class="textoCuentasRojo col-sm-3" style="text-align: right;">  <span> {{ _formatFix2(item.balance)}} € </span> </div>
            </template>
            <div class="col-lg-1" style="text-align: right;">
              <template is="dom-if" if="{{_esBorrable(item.balance)}}">
                <iron-icon icon="delete-forever"   style="fill:blue" title="eliminar cuenta"   on-click="openConfirm" ></iron-icon>
              </template>
            </div>
            <div class="col-lg-1" style="text-align: right;">
            <iron-icon icon="visibility"   style="fill:blue" title="ver lista operaciones" on-click="consultaMovimientos"></iron-icon> </div>
            <div class="col-lg-1" style="text-align: right;">  <iron-icon icon="add-circle-outline"  style="fill:blue" title="realizar una operación" on-click="irAltaMovimiento"></iron-icon> </div>
          </div>
         </div>
      </template>
    </dom-repeat>
</div>

</div>
<div class="col-7 ">
   <visor-movimientos id="verMovimientos" hidden$="{{!verMov}}"> </visor-movimientos>
 </div>
</div>

</div>

</div>


<div style=" padding-top: 100px; margin-left:400px; margin-right:400px " hidden$="{{showErrorAuth}}">
  <div class="row caja" style="justify-content: center; height: 110px;" >
    <div style="justify-content: center;"  >
      <span class="redG">Acceso no autorizado</span>
    </div>
  </div>
</div>


<ventana-avisos id="confirm" on-myevent="_aceptarConfirm"></ventana-avisos>

      <iron-ajax

      id="altaCuenta"
      url="http://localhost:3000/techUProyecto/cuentas/{{iduser}}"
      method="POST"
      handle-as="json"
      on-response="recuperaCuentas"
      ></iron-ajax>

      <iron-ajax
      id="getAccount"
      url="http://localhost:3000/techUProyecto/cuentas/{{iduser}}"
      handle-as="json"
      content-type="application/json"
      on-response="showData"
      on-error="showError"
      ></iron-ajax>

      <iron-ajax
      id="deleteCuenta"
      url="http://localhost:3000/techUProyecto/cuentas/{{iduser}}/{{IBAN}}"
      handle-as="json"
      method="DELETE"
      on-response="recuperaCuentas"
      on-error="showError"
      ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      iban: {
        type: String
      }, iduser: {
        type: String,
        value:window.location.search.substring(4)
      }, balance: {
        type: Number
      }, accounts: {
        type: Array
      }, movimientos: {
        type: Array
      }, verCuentas:{
        type: Boolean,
        value: false
      }, verMov: {
        type: Boolean,
        value: false
      }, showErrorAuth : {
        type:Boolean,
        value : true
      }, altaMovimiento : {
        type : Boolean,
        value : false,
        observer : "_altaOk"
      }, accionOption : {
        type : String,
        value :""
      }
    };
  }// end properties


//funcion que se ejecuta cuando se termina de cargar la pagina, con esto evito el auto y puedo prepara el req a enviar
  ready() {
    super.ready();
    this.verMov=false;
    this.$.verMovimientos.IBAN="";
    if(sessionStorage.getItem("Authorization")){
      this.$.getAccount.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.getAccount.generateRequest();
    this.altaMovimiento=false;

  }//ready() {

  openConfirm(e){
    this.$.confirm.textoConfirmacion1="";
    this.$.confirm.textoConfirmacion2="";
    this.accionOption="";

    //Si no llega iban vamos a alta. Caso contrario vamos a eliminar
    if (e.model!=null && e.model.item.IBAN!=""){
      this.IBAN = e.model.item.IBAN;
      this.$.confirm.textoConfirmacion1="Se va a proceder a eliminar la cuenta "+this.IBAN;
      this.$.confirm.textoConfirmacion2="¿Estás seguro?";
      this.accionOption="baja";
      this.$.confirm.shadowRoot.getElementById("animated").open();

    } else {
      this.$.confirm.textoConfirmacion1="Se va a crear una nueva cuenta. Tendrá un saldo inicial de 0,00 €";
      this.$.confirm.textoConfirmacion2="¿Estás seguro?";
      this.accionOption="crear";
      this.$.confirm.shadowRoot.getElementById("animated").open();
    }
  }//  openConfirm(e){

    openAlert(){
      this.$.confirm.textoConfirmacion1="";
      this.$.confirm.textoConfirmacion2="";
      this.accionOption="";
      this.$.confirm.textoConfirmacion1="Se ha realizado la operación correctamente";
      this.$.confirm.shadowRoot.getElementById("alert").open();
    }//  openAlert(){

    _aceptarConfirm(e){
      //segun la acción aceptada se vrea o borra una cuenta
      if (this.accionOption=="crear"){
        //vamos a crear cuenta
        this.accionOption="";
        this.crearCuenta();
      } else if (this.accionOption="baja"){
        //vamos a eliminar cuenta
        this.accionOption="";
        this.eliminarCuenta();
      }


    }//_aceptarConfirm(){



  _altaOk(newValue, oldValue) {

      if (newValue) {
        if(sessionStorage.getItem("Authorization")){
          this.$.getAccount.headers.authorization = sessionStorage.getItem("Authorization");
        }
        this.$.getAccount.generateRequest();
        this.altaMovimiento=false;
        this.$.verMovimientos.IBAN="";
      }
  }//_altaOk(newValue, oldValue) {

    eliminarCuenta(){
     this.verMov=false;//ocultar movimientos
      if(sessionStorage.getItem("Authorization")){
        this.$.deleteCuenta.headers.authorization = sessionStorage.getItem("Authorization");
      }
      this.$.deleteCuenta.generateRequest();

    }//eliminarCuenta(){


  crearCuenta (){
    this.verMov=false;//ocultar movimientos

    if(sessionStorage.getItem("Authorization")){
      this.$.altaCuenta.headers.authorization = sessionStorage.getItem("Authorization");
    }
    this.$.altaCuenta.generateRequest();

  }//crearCuenta (){

  recuperaCuentas(data) {
      this.openAlert();
      this.altaMovimiento=true;

  }//recuperaCuentas(data) {

  consultaMovimientos(e) {
    this.$.verMovimientos.IBAN=e.model.item.IBAN;
    this.IBAN = e.model.item.IBAN;
    this.verMov=true;
  }//consultaMovimientos(e) {

  showData(data) {
    this.accounts = data.detail.response;
  }//showData(data) {

  irAltaMovimiento(e) {
    // //e es un parámetro que está en las funciones y tiene información del evento e.model.item.IBAN
    this.dispatchEvent(
          new CustomEvent(
        "myevent",
        {
          "detail" : {
            "IBAN" : e.model.item.IBAN,
            "selPagina" : "alta-movimiento"
            }
        }
      )
    )
  } //irAltaMovimiento(e) {

  //captura el error definido en el iron ajax
  showError(error)
  {
    this.verMov=false;

    //Si el error es 401  no autorizado
    if (error.detail.request.status==401 || error.detail.request.status==403){
     this.showErrorAuth=false;
   }

 }//showError(error)

  _formatFix2(valor){
    valor = new Intl.NumberFormat("es-ES").format(valor);
    if (valor.indexOf(",")==-1){
        valor =   valor + ',00';
    }else {
      //miro los decimales
      if (valor.toString().length-valor.indexOf(",")<3)
      {
        valor =   valor + '0';
      }
    }
    return valor
  }//_formatFix2(valor){

  _esBorrable(saldo){
    if (saldo ==0){
      return true;
    } else {
      return false;
    }

  }//_esBorrable(saldo){

  _saldoNegativo(saldo){

    if (saldo < 0){
      return true;
    } else {
      return false;
    }

  }//_saldoNegativo(saldo){

}//end class

window.customElements.define('visor-cuentas', VisorCuentas);
