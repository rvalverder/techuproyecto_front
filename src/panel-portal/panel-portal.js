import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '../logout-usuario/logout-usuario.js';
import '../visor-usuario/visor-usuario.js';
import '../visor-cuentas/visor-cuentas.js';
import '../alta-usuario/edicion-usuario.js';

/**
 * @customElement
 * @polymer
 */
class HomePortal extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
        .body {
          margin: 0px;
        }
        .cabecera {
            background:  #004481;
            height: 100px;
            margin:0px;
            border:0px;
            justify-content: center;
            align-items: center;
            width: 99%;
            margin: 0 auto;

        }

        .bluebg {
          background-color: #004481;

        }

        .enlacesCabecera{
          cursor:pointer;
          font-size: 17px;
          font-weight: bold;
          color: #ffffff;
          font-family: arial;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <div class="bluebg">
          <div class="row cabecera">
            <div class="col-2  col-sm-6 " >
              <img src="/src/img/techu.jpg" />
            </div>
            <div class="col-3 offset-3 col-sm-1 ">
              <span  class="enlacesCabecera" on-click="openHelp"> Ayuda</span>
            </div>
            <div class="col-2 col-sm-1 ">
              <span class="enlacesCabecera" on-click="editarPerfil" > Mi perfil</span>
            </div>
            <div class="col-3 col-sm-1 ">
            <span class="enlacesCabecera"  on-click="logout" > Logout</span>
            </div>
          </div>
      </div>





      <iron-pages selected="[[selPagina]]" attr-for-selected="component-name">
          <div component-name="user-cuentas">
            <visor-usuario id="datosUsuario"></visor-usuario>
            <visor-cuentas on-myevent="irAltaMovimiento" id="cuentas"></visor-cuentas></div>
          </div>
          <div component-name="alta-movimiento"> <alta-movimiento on-myevent="vueltaAltaMov" id="altaMov"></alta-movimiento></div>
          <div component-name="logout-usuario"> <logout-usuario on-myevent="cancelarLogout" ></logout-usuario></div>
          <div component-name="editar-usuario" id="editarUsu"> <editar-usuario on-myevent="cancelarEdicion" ></editar-usuario></div>
      </iron-pages>


  `;
  }

  static get properties() {
    return {
      selPagina : {
        type : String,
        value : "user-cuentas"
      }
    }
    }// end properties

  logout(){
    this.selPagina = "logout-usuario";
  }

  cancelarLogout(e){
      this.selPagina = "user-cuentas";

  }
  cancelarEdicion(e){
    this.$.datosUsuario.altaMovimiento=true;
    this.selPagina = "user-cuentas";
  }

  irAltaMovimiento(e){
    this.$.altaMov.IBAN = e.detail.IBAN;
    this.$.altaMov.altaOk=false;
    this.$.altaMov.ready();
    this.selPagina = "alta-movimiento";

  }

  vueltaAltaMov(e){
    this.$.cuentas.verMov=false;
    this.$.cuentas.IBAN="";
    this.$.cuentas.altaMovimiento=true;
    this.$.datosUsuario.altaMovimiento=true;
    this.$.datosUsuario.mes="";
    this.selPagina = "user-cuentas";
  }


  editarPerfil(){
    this.selPagina = "editar-usuario";
  }

 openHelp(e){

      window.open("/src/hlp/TechU-Bank Manual de Usuario.pdf")
  }

}//end class

window.customElements.define('panel-portal', HomePortal);
