import '@polymer/iron-ajax/iron-ajax.js';
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';


/**
 * @customElement
 * @polymer
 */
class AltaMovimiento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .azulG {
            color: #004481;
            font-weight: bold;
            font-size: 24px;
        }

        .verdeG {
            color: #268100;
            font-weight: bold;
            font-size: 24px;
        }
        .azulLabel {
            color: #004481;
            font-weight: bold;
            font-size: 16px;
        }
        .verdeLabel {
            color: #268100;
            font-weight: bold;
            font-size: 16px;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .caja {
            height: 60px;
            border-radius: 10px;
            border: 1px solid #dddfe2;
            justify-content: center;
            align-items: center;
            margin-left: 30px;


          }
      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<form class="was-validated">

  <div style="padding-top: 5px; margin-left:400px; margin-right:400px" >
    <div class="row caja" style="background-color:#d4d3ea">
      <span class="azulG">alta movimiento</span>
    </div>
    <div style="padding-top: 30px; margin-left:100px; margin-right:100px" hidden$="{{altaOk}}">
      <div class="mb-4" >
        <label class="azulLabel" for="validationServer03">cuenta origen</label>
        <input type="text" class="form-control" id="validationServer03" readonly placeholder="cuenta origen" required value="{{IBAN::input}}"></input>
      </div>

      <div class="form-group">
          <label class="azulLabel" for="validationServer03">tipo de movimiento</label>
          <select class="custom-select" required value="{{tipoMovimiento::change}}" on-change="validaTipoMov">
            <option value="">Selecciona tipo movimiento </option>
            <optgroup label="ABONOS">
            	<option value="Abono de dividendos">Abono de dividendos </option>
            	<option value="Abono intereses">Abono intereses </option>
            	<option value="Abono nómina">Abono nómina </option>
            	<option value="Ingreso Efectivo">Ingreso Efectivo </option>
            	<option value="Otros abonos">Otros abonos </option>
              <option value="Solicitar divisa extranjera">Solicitar divisa extranjera </option>
            </optgroup>
            <optgroup label="CARGOS">
            	<option value="Cargo domiciliación">Cargo domiciliación </option>
            	<option value="Disposición de efectivo">Disposición de efectivo</option>
              <option value="Ingresar divisa extranjera">Ingresar divisa extranjera </option>
              <option value="Otros cargos">Otros cargos</option>
            	<option value="Pago tributos">Pago tributos</option>
            	<option value="Transferencia otra cuenta">Transferencia otra cuenta</option>

            </optgroup>
          </select>
        </div>

        <div class="mb-4" hidden$="{{!mostrarCtaDestino}}">
          <label class="azulLabel" for="validationServer03">cuenta destino</label>
          <input type="text" class="form-control is-invalid" id="validationServer03" placeholder="cuenta destino" required value="{{IBANdestino::input}}"></input>
        </div>


        <div class="mb-4" >
          <label class="azulLabel" for="validationServer03">importe {{divisaOrigen}} [0.00]</label>
          <input type="Number"  class="form-control is-invalid" id="validationServer03" placeholder="Importe" min="1" step=".01" required value="{{importe::input}}"></input>
        </div>

        <div class="mb-4" hidden$="{{!mostrarDivisas}}">
          <label class="azulLabel" for="validationServer03">Divisa extranjera</label>
          <select class="custom-select" required value="{{divisaSelec::change}}" on-change="validaDivisa" disabled$={{!activarDivisas}}>
            <option value="">Selecciona Divisa </option>
            <option value="ARS">ARS - Peso argentino </option>
            <option value="COP">COP - Peso colombiano </option>
            <option value="MXN">MXN - Peso mexicano </option>
            <option value="PEN">PEN - Sol peruano </option>
            <option value="TRY">TRY - Lira turca </option>
            <option value="USD">USD - Dólar de los Estados Unidos </option>
            <option value="UYU">UYU - Peso uruguayo </option>
          </select>
        </div>

        <div class="mb-4" hidden$="{{!mostrarDivisas}}">
          <label class="azulLabel" for="validationServer03">importe en {{divisaDestino}}</label>
          <input type="Number"  class="form-control " id="validationServer03" readonly placeholder="Importe Divisa Calculado" required value="{{importeDivisa::input}}"></input>
        </div>

        <div class="mb-4" >
          <label class="azulLabel" for="validationTextarea">concepto</label>
            <textarea class="form-control" id="validationTextarea" readonly$="{{mostrarDivisas}}" placeholder="concepto de la operación" required  value="{{concepto::input}}"></textarea>
        </div>
      </div>
        <div class="mb-4" hidden$="{{altaOk}}">
          <div class="row caja" >
              <div class="col-3" >
                <button type="button" class="btn btn-outline-secondary" on-click="volver">cancelar</button>
              </div>
              <div class="col-2" >
                <button type="button" class="btn btn-outline-success" on-click="aceptar">aceptar</button>
              </div>
          </div>
        </div>

    </div>
  </div>



  <div style="padding-top: 10px;  margin-left:400px; margin-right:400px" height=800px  >
    <div class="row" style="background-color:#d4edda; height=400px; " hidden$="{{!altaOk}}">
      <div   >
          <div class="mb-6" style="padding-top: 30px; padding-bottom: 20px; margin-left:150px; margin-right:60px; " >
            <span class="verdeG">¡Operación realizada satisfactoriamiente!</span>
          </div>
      </div>
      <div class="mb-6" style="padding-top: 20px; padding-bottom: 50px; margin-left:100px; margin-right:100px"  >
        <span class="verdeLabel">Se ha realizado el {{tipoMovimiento}} de {{importe}} € correctamente. Pulse en el botón volver regresar a consultar sus cuentas  </span>
      </div>
    </div>
</div>

<div style="padding-top: 10px; margin-left:400px; margin-right:400px" hidden$="{{!altaOk}}" >

    <div class="row caja" style="padding-top: 10px; justify-content: center;" >
          <button type="button" class="btn btn-outline-primary" on-click="volver">volver</button>
    </div>
</div>


</form>



    <iron-ajax
      id="crearMovimiento"
      url="http://localhost:3000/techUProyecto/movimientos/{{iduser}}"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>

      <iron-ajax
        id="mostrarCambio"
        url="http://localhost:3000/techUProyecto/cambio/{{divisaOrigen}}/{{divisaDestino}}/{{importe}}"
        handle-as="json"
        method="GET"
        content-type="application/json"
        on-response="showCambio"
        on-error="showError"
        ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      IBAN : {
        type: String
      },
      IBANdestino:{
        type: String,
        value : ""
      },
      importe: {
        type: Number
        ,
        observer : "_importeChanged"
      }, importeDestino:{
        type:Number
      },
      tipoMovimiento: {
        type: String,
        value : "",
        observer : "_typeChanged"
      },
      concepto: {
        type: String,
        value : ""
      }, traspasoDestino:{
        type:Boolean,
        value: false
      }, mostrarCtaDestino:{
        type:Boolean,
        value: false
      },
      altaOk: {
        type: Boolean,
        value: false
      }, iduser: {
        type: String,
        value:window.location.search.substring(4)
      }, mostrarDivisas:{
        type:Boolean,
        value: false
      },
      activarDivisas:{
        type:Boolean,
        value: false
      }, divisaSelec:{
        type: String,
        observer : "_divisaChanged",
        value : ""
      }, importeDivisa:{
        type: Number
      },
      divisaOrigen: {
        type: String,
        value : "EUR"
      },
      divisaDestino: {
        type: String,
        value : ""
      }

    };
  }// end properties


  ready() {
     super.ready();
     this.traspasoDestino=false;
     this.IBANdestino="";
     this.importe="";
     this.tipoMovimiento="";
     this.concepto="";
     this.importeDestino=0;
  }

  validaTipoMov(){
    if (this.tipoMovimiento=="Transferencia otra cuenta"){
      this.mostrarCtaDestino=true;
    } else {
      this.mostrarCtaDestino=false;
    }
    if (this.tipoMovimiento=="Solicitar divisa extranjera"){
      this.mostrarDivisas=true;
      this.divisaOrigen="EUR";

    } else {
      this.mostrarDivisas=false;
      this.divisaOrigen="EUR";

    }
    if (this.tipoMovimiento=="Ingresar divisa extranjera"){
      this.mostrarDivisas=true;
      this.divisaDestino="EUR";
      this.divisaOrigen="";

    } else {
      this.mostrarDivisas=false;
      this.divisaOrigen="EUR";

    }

  }

  validaDivisa(){
    console.log("Has cambiado la divisa #" + this.divisaSelec+"#");
  }

_divisaChanged(newValue, oldValue) {
    if(newValue){
      if(this.tipoMovimiento=="Solicitar divisa extranjera")  {
        this.divisaOrigen="EUR";
        this.divisaDestino=newValue;
      }else{
        this.divisaDestino="EUR";
        this.divisaOrigen=newValue;
      }
      this.$.mostrarCambio.generateRequest(); //esto es para ejecutar la función.
    }
}
_importeChanged(newValue, oldValue) {

    // si cambia el importe y estoy en divisas tengo que validar que hay importe para activar combo
    if(this.tipoMovimiento=="Solicitar divisa extranjera" || this.tipoMovimiento=="Ingresar divisa extranjera"){
      if(newValue && newValue!=null && newValue!="" ){
        this.activarDivisas=true;
        if (this.tipoMovimiento=="Solicitar divisa extranjera" ){
              this.divisaOrigen="EUR";
        } else {
              this.divisaOrigen="EUR";
        }
        if (this.tipoMovimiento=="Ingresar divisa extranjera"){
          this.divisaDestino="EUR";
          this.divisaOrigen="";
        } else {
          this.divisaOrigen="€";
        }
        if (this.divisaSelec && this.divisaSelec!=null && this.divisaSelec!="" && this.divisaDestino!="" &&this.divisaOrigen!=""){
          // si cambia el importe y hau una divisa seleccionada ademas tengo que recalcular
          this.$.mostrarCambio.generateRequest();
        }
      }else{
        this.activarDivisas=false;
        this.importeDivisa="";
        this.concepto="";
      }
    }
}

  //captura la respuesta del iron ajax
  showCambio(data) {
    //this.movimientos = data.detail.response;
    this.importeDivisa = data.detail.response.result.amount;

    this.concepto = "Importe "+this.importe+" "+this.divisaOrigen+" en la moneda "+this.divisaDestino+", el cambio a fecha "
    +data.detail.response.result.updated+" es de "+ data.detail.response.result.value
    +". Movieminto de "+data.detail.response.result.amount+" "+data.detail.response.result.target;
  }

  _typeChanged(newValue, oldValue) {
      if (newValue == "Transferencia otra cuenta")
      {
          this.mostrarCtaDestino=true;
      } else {
          this.mostrarCtaDestino=false;
      }

      if (newValue == "Solicitar divisa extranjera" || this.tipoMovimiento=="Ingresar divisa extranjera")
      {
          this.mostrarDivisas=true;
          if (newValue == "Solicitar divisa extranjera"){
            this.divisaOrigen="EUR";
            this.divisaDestino="";
            this.concepto="";
            this.importe="";
            this.divisaSelec="";
            this.importeDivisa="";
          }else{
            this.divisaOrigen="";
            this.divisaDestino="EUR";
            this.concepto="";
            this.importe="";
            this.divisaSelec="";
            this.importeDivisa="";
          }

      } else {
          this.mostrarDivisas=false;
          this.divisaOrigen="EUR";
          this.concepto="";
          this.importe="";
          this.divisaSelec="";
          this.importeDivisa="";
      }

    }

  volver () {
       this.dispatchEvent(
         new CustomEvent(
           "myevent", {}
         )
       )
  }

  aceptar (){
    this.importeDestino = this.importe;
    //si todos los campos estan rellenos
    if (this.IBAN!="" && this.importe!=0 && this.tipoMovimiento!="" && this.concepto!="")
    {
        //SEGÚN EL TIPO DEMOVIMIENTO CAMBIAMOS EL IMPORTE.
        switch (this.tipoMovimiento) {
          case "Cargo domiciliación":
            this.importe = this.importe*(-1);
            break;
          case "Disposición de efectivo":
            this.importe = this.importe*(-1);
            break;
          case "Pago tributos":
            this.importe = this.importe*(-1);
            break;
          case "Otros cargos":
            this.importe = this.importe*(-1);
            break;
          case "Transferencia otra cuenta":
            this.importe = this.importe*(-1);
            break;
          case "Solicitar divisa extranjera":
            this.importe = this.importe*(-1);
            break;
          case "Ingresar divisa extranjera":
            this.importe = this.importeDivisa;
            break;
          default:
            this.importe = this.importe;
            break;
          }

          var loginData = {
            "IBAN" : this.IBAN,
            "importe" : parseFloat(parseFloat(this.importe).toFixed(2)),
            "tipoMovimiento" : this.tipoMovimiento,
            "concepto" : this.concepto
          }

          //si es transferencia actualizamos a true para que haga las dos
          if (this.tipoMovimiento=="Transferencia otra cuenta"){
            this.traspasoDestino=true;
          }
          if(sessionStorage.getItem("Authorization")){
            this.$.crearMovimiento.headers.authorization = sessionStorage.getItem("Authorization");
          }

          this.$.crearMovimiento.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
          this.$.crearMovimiento.generateRequest(); //esto es para ejecutar la función.

      }//fin if validación
  }

//alta 2 movimiento en transferenciaaceptar (){
  altaTransferencia(){
  this.traspasoDestino=false;

  var loginData = {
      "IBAN" : this.IBANdestino,
      "importe" : parseFloat(parseFloat(this.importeDestino).toFixed(2)),
      "tipoMovimiento" : this.tipoMovimiento,
      "concepto" : this.concepto
    }

    if(sessionStorage.getItem("Authorization")){
      this.$.crearMovimiento.headers.authorization = sessionStorage.getItem("Authorization");
    }

    this.$.crearMovimiento.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
    this.$.crearMovimiento.generateRequest(); //esto es para ejecutar la función.

 }

  //captura el error definido en el iron ajax
  showError(error)
  {
    console.log("algo ha ido mal amigo");
    console.log(error);



  }

  //captura la respuesta del iron ajax
  manageAJAXResponse(data) {
    //si hay que hacer la otra transferencia
    if(this.traspasoDestino){
      this.traspasoDestino=false;
      this.altaTransferencia();
    }
    this.altaOk = true;
  }
}//end class

window.customElements.define('alta-movimiento', AltaMovimiento);
