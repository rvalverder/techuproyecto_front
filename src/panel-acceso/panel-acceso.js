import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../funciones-comunes/visor-cambio.js'

/**
 * @customElement
 * @polymer
 */
class PanelAcceso extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .box-card {
            margin-top:80px;
            background: #fff;
            height: 250px;
            border-radius: 10px;
            border: 1px solid #dddfe2;

        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .detalle_box {
            background: transparent;
            font-size: 18px;
            padding: 15px;
            font-weight: bold;
            color: green;
            font-family: arial;
        }
        .head_box {
            background: transparent;
            font-size: 28px;
            padding: 15px;
            font-weight: bold;
            justify-content: center;
            align-items: center;
            color: #0072c9;
            font-family: arial;
        }
        .body_box {
            background: transparent;
            font-size: 16px;
            padding: 15px;
            align-items: center;
            font-weight: bold;
            color: #004481;
            font-family: arial;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<div style="padding-top: 15px; margin-left:40px; margin-right:40px" >
  <div class="row" style="margin-left:240px; margin-right:40px">
    <div class="col-6">
      <iframe src="https://www.tutiempo.net/s-widget/app/?LocId=3768&sc=1" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:411px;" allowTransparency="true"></iframe>
    </div>



    <div class="col-5 box-card">
          <div class="row  " style=" margin-left:20px;" >
             <span class="head_box"> TechU Bank </span>
          </div>
          <div class="row " style="margin-left:20px;" >
            <span class="body_box"> Aplicación realizada para la Tech University Practitioner v.11 por</span>
          </div>
          <div class="row detalle_box" style="margin-left:60px;" >
            <iron-icon icon="account-box"  style="fill:green"></iron-icon>
            <span style="margin-left:10px;"> Sandra Fernandez Ballesteros</span>
          </div>
          <div class="row detalle_box " style="margin-left:60px;" >
            <iron-icon icon="account-box"  style="fill:green"></iron-icon>
            <span style="margin-left:10px;"> Raúl Valverde Ruiz</span>
          </div>
      </div>
    </div>




  <div class="row">
    <div class="col-sm-12  text-center box-area">
      <visor-cambio></visor-cambio>
    </div>

  </div>
</div>










  `;
  }

  static get properties() {
    return {

    };
  }// end properties



}//end class

window.customElements.define('panel-acceso', PanelAcceso);
